import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'temperature-converter',
  templateUrl: './temperatureConverter.component.html',
  styleUrls: ['./temperatureConverter.component.scss']
})

export class TemperatureConverter implements OnInit {
  fVal: number;
  cVal: number;
  ngOnInit() {
    // C = (F − 32) × 5/9
    // F = C*9/5 + 32
  }

  calculateCelsius(e) {
    this.cVal = (this.fVal - 32) * (5/9)
    this.cVal = parseFloat(this.cVal.toFixed(1))
  }

  calculateFahrenheit(e) {
    this.fVal = (this.cVal*(9/5)) + 32
    this.fVal = parseFloat(this.fVal.toFixed(1))
  }

}
