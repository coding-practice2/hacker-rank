'use strict'

function sockMerchant(n, ar) {
    let stacks = {}
    let pairs = 0
    for (let i = 0; i < n; i++) {
        if (ar[i] in stacks) {
            stacks[ar[i]]++
        } else {
            stacks[ar[i]] = 1
        }
    }

    for (let color in stacks) {
        pairs += Math.floor(stacks[color]/2)
    }

    return pairs
}

let n = 9
let ar = [10,20,20,10,10,30,50,10,20]
sockMerchant(n, ar)
