'use strict'

function repeatedString(s, n) {
    let charToFind = 'a'
    let repeat = Math.floor(n/s.length)
    console.log('Repeat: ', repeat)
    let lastPieceLength = n%s.length
    console.log('Last Piece: ', lastPieceLength)
    let count = 0;

    count += repeat * getOccurrence(s, charToFind)

    s = s.substr(0,lastPieceLength)
    count += getOccurrence(s, charToFind)

    console.log('Occurrence', count)

    return count
}

function getOccurrence(s, c) {
    let occurrence = 0
    for (let i=0;i<s.length;i++) {
        if(s[i] === c) { occurrence++ }
    }
    return occurrence
}

repeatedString("a", 100000000000)
