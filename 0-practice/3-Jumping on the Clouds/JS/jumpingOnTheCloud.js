'use strict'

/**
 * PROBLEM: Jumping on the Clouds
 *
 * @param c Array of clouds
 * @returns {number} of jumps
 */

function jumpingOnClouds(c) {
    let size = c.length
    let jumps = 0

    let i = 0
    while(i < size - 1) {
        if (c[i+2] === 0) {
            i = i + 2
            jumps++
        } else {
            i++
            jumps++
        }
    }
    return jumps
}

let c = [0,0,0,0,1,0]
jumpingOnClouds(c)
