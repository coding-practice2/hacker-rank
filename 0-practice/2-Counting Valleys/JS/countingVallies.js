function countingValleys(steps, path) {
    let level = 0
    let valleys = 0
    for (let i = 0; i < steps; i++) {
        if(path[i] == 'U') { level++ }
        else if(path[i] == 'D') { level-- }

        if (level == 0 && i > 0) {
            valleys = path[i] == 'U' ? ++valleys : valleys
        }
    }

    return valleys
}

const steps = 8;
const path = "UDDDUDUU";

countingValleys(steps, path)
